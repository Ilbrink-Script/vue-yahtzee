export default {
    addScore: (state, score) => state.scores.push(score),
    setScores: (state, scores) => state.scores = scores,
    setDices: (state, dices) => state.dices = dices,
    rollDices: state => {
        state.dices.forEach(dice => {
            if (!dice.locked) {
                dice.eyes = Math.ceil(Math.random() * 6);
            }
        })
        state.rollCount++;
    },
    toggleDice: (state, id) => {
        if (state.rollCount > 0)
            state.dices[id].locked = !state.dices[id].locked
    },
    resetRoll: state => {
        state.dices.forEach(dice => {
            dice.eyes = 0;
            dice.locked = false;
        });
        state.rollCount = 0;
    },
    resetScores: state => {
        state.scores.forEach(score => {
            score.locked = false;
            score.points = 0;
        });
    },
    lockScorePoints: (state, payload) => {
        const score = state.scores[payload.idx];
        score.points = payload.points;
        score.locked = true;
    },
}