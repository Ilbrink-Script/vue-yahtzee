export { helpers };

const helpers = {
    numberToText: number => {
        if (!isNaN(+number)) {
            number = +number;
        }

        switch (number) {
            case 1: return "one";
            case 2: return "two";
            case 3: return "three";
            case 4: return "four";
            case 5: return "five";
            case 6: return "six";
            default: return "-";
        }
    },

    buildScore: (scoreTemplate, scoreExtension) => {
        const score = Object.assign({}, scoreTemplate, scoreExtension);
        if (score.type === 'numeric') {
            score.description = `Total ${score.title}'s`;
        } else if (score.fixedPoints > 0) {
            score.description = `${score.fixedPoints} points`;
        } else {
            score.description = 'Total dice';
        }
        return score;
    },
};

export default {
    methods: {
        faDice: eyes => {
            let result = "fa-dice";
            if (eyes > 0) {
                result += "-" + helpers.numberToText(eyes);
            }
            return result;
        },
    }
}
