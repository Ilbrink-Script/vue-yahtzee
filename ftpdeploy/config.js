module.exports = {
  localBasePath: "/",
  remoteBasePath: "/vue-yahtzee",
  sync: [
    { src: "/dist", dest: "/" }
  ],
};
