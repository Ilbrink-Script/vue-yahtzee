export default {
    unlockedDices: state => {
        return state.dices.filter(dice => !dice.locked)
    },
    aggregateDices: state => {
        const aggrDices = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0 };
        state.dices.forEach(dice => {
            if (dice.eyes > 0)
                aggrDices[dice.eyes]++
        });
        return aggrDices;
    },
    canRoll: (state, getters) => {
        const MAX_ROLLS = 3; // 99 for testing
        return state.rollCount < MAX_ROLLS && !getters.gameOver;
    },
    canScore: (state, getters) => getters.unlockedScores.length && state.rollCount,
    gameOver: (state, getters) => getters.unlockedScores.length === 0,
    numericPoints: (state, getters) => eyes => {
        return getters.aggregateDices[eyes] * eyes;
    },
    aggregatePoints: (state, getters) => (minDuplicates, fixedPoints) => {
        let maxDuplicates = 0;
        let sum = 0;
        for (const [num, duplicates] of Object.entries(getters.aggregateDices)) {
            sum += num * duplicates;
            maxDuplicates = Math.max(maxDuplicates, duplicates);
        }

        const enoughDuplicates = maxDuplicates >= minDuplicates;
        const tmpPoints = fixedPoints > 0 ? fixedPoints : sum;
        const points = enoughDuplicates ? tmpPoints : 0;

        return points;
    },
    fullHousePoints: (state, getters) => (fixedPoints) => {
        const diceDuplicates = Object.values(getters.aggregateDices);

        const double = diceDuplicates.includes(2);
        const triple = diceDuplicates.includes(3);
        const yahtzee = diceDuplicates.includes(5)

        const points = (double && triple || yahtzee) ? fixedPoints : 0;

        return points;
    },
    streetPoints: (state, getters) => (minConsecutive, fixedPoints) => {
        let consecutive = 0;
        let maxConsecutive = 0;

        const diceOccurances = Object.values(getters.aggregateDices);

        // loop through the dice occurances
        diceOccurances.forEach(diceCount => {
            if (diceCount > 0) {
                consecutive++;
                maxConsecutive = Math.max(consecutive, maxConsecutive);
            } else {
                consecutive = 0;
            }
        });

        // check if enough consecutive dices were found
        const points = maxConsecutive >= minConsecutive ? fixedPoints : 0;
        // console.log('street points', minConsecutive, fixedPoints, points);
        return points;
    },
    scoresByTitle: state => title => state.scores.filter(score => score.title === title),
    scoresByType: state => type => state.scores.filter(score => score.type === type),
    scoreIdx: state => title => state.scores.findIndex(score => score.title === title),
    unlockedScores: state => state.scores.filter(score => !score.locked),
    numericTotal: (state, getters) => getters.scoresByType('numeric').reduce((sum, numScore) => sum + numScore.points, 0),
    bonus: (state, getters) => {
        const BONUS_LIMIT = 63;
        const BONUS_POINTS = 35;
        return getters.numericTotal >= BONUS_LIMIT ? BONUS_POINTS : 0;
    },
    upperTotal: (state, getters) => getters.numericTotal + getters.bonus,
    upperScores: (state, getters) => getters.scoresByType('numeric'),
    bottomScores: (state, getters) => {
        const aggrScores = getters.scoresByType('aggregate');
        return [
            ...aggrScores.slice(0, 2),
            ...getters.scoresByType('fullHouse'),
            ...getters.scoresByType('street'),
            ...aggrScores.slice(-2),
        ];
    },
    bottomTotal: (state, getters) => getters.bottomScores.reduce((sum, bottomScore) => sum + bottomScore.points, 0),
    grandTotal: (state, getters) => getters.upperTotal + getters.bottomTotal,
    sortedDices: state => [...state.dices].sort((a, b) => {
        const sumEyes = a.eyes - b.eyes;
        if (sumEyes === 0 && !a.locked) {
            return -1;
        }
        return sumEyes;
    }),
};