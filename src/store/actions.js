import { helpers } from '@/mixins/helpers';

export default {
    setupScores({ state, commit }) {
        // don't execute twice, how to do this properly?
        if (state.scores.length > 0) {
            return;
        }

        const scores = [];

        // base score object
        const scoreObj = {
            fixedPoints: false,
            points: 0,
            locked: false,
        };

        // numeric scores
        for (let i = 1; i <= 6; i++) {
            // scores.push(this.helpers.buildScore(scoreObj, { // fix this to use helpers mixin ?
            scores.push(helpers.buildScore(scoreObj, {
                title: i,
                example: i,
                type: 'numeric',
            }));
        }

        // aggregate scores
        scoreObj.type = 'aggregate';
        scores.push(helpers.buildScore(scoreObj, {
            title: 'Three of a kind',
            example: 333,
            minDuplicates: 3,
        }));
        scores.push(helpers.buildScore(scoreObj, {
            title: 'Four of a kind',
            example: 4444,
            minDuplicates: 4,
        }));

        // full house score
        scores.push(helpers.buildScore(scoreObj, {
            title: 'Full House',
            example: '22+333',
            type: 'fullHouse',
            fixedPoints: 25,
        }));

        // street scores
        scoreObj.type = 'street';
        scores.push(helpers.buildScore(scoreObj, {
            title: 'Small street',
            example: 1234,
            minConsecutive: 4,
            fixedPoints: 30,
        }));
        scores.push(helpers.buildScore(scoreObj, {
            title: 'Large street',
            example: 12345,
            minConsecutive: 5,
            fixedPoints: 40,
        }));

        // commit whole score array
        commit('setScores', scores);

        // more aggregate scores (appending to existing scores)
        scoreObj.type = "aggregate";
        commit('addScore', helpers.buildScore(scoreObj, {
            title: 'Yahtzee',
            example: 55555,
            minDuplicates: 5,
            fixedPoints: 50,
        }));
        commit('addScore', helpers.buildScore(scoreObj, {
            title: 'Chance',
            minDuplicates: 0,
        }));
    },
    setupDices: ({ commit }) => {
        const dices = [];
        for (let i = 0; i < 5; i++) {
            dices.push({
                id: i,
                eyes: 0,
                locked: false,
            });
        }
        commit('setDices', dices);
    },
    setup: ({ dispatch }) => {
        dispatch('setupScores');
        dispatch('setupDices');
    },
    roll: ({ commit, getters }) => {
        if (getters.canRoll) {
            commit('rollDices');
        }
    },
    lockScore: ({ commit, getters }, score) => {
        const scoreIdx = getters.scoreIdx(score.title);
        let points;
        switch (score.type) {
            case 'numeric':
                points = getters.numericPoints(score.title);
                break;
            case 'aggregate':
                points = getters.aggregatePoints(score.minDuplicates, score.fixedPoints);
                break;
            case 'fullHouse':
                points = getters.fullHousePoints(score.fixedPoints);
                break;
            case 'street':
                points = getters.streetPoints(score.minConsecutive, score.fixedPoints);
                break;
        }

        commit('lockScorePoints', {
            idx: scoreIdx,
            points: points,
        });
        commit('resetRoll');
    },
    resetGame: ({ commit }) => {
        commit('resetScores');
        commit('resetRoll');
    },
}